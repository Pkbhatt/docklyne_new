<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

    
if ( !function_exists( 'wpestate_chld_thm_cfg_parent_css' ) ):
   function wpestate_chld_thm_cfg_parent_css() {

    $parent_style = 'wpestate_style'; 
    wp_enqueue_style('bootstrap',get_template_directory_uri().'/css/bootstrap.css', array(), '1.0', 'all');
    wp_enqueue_style('bootstrap-theme',get_template_directory_uri().'/css/bootstrap-theme.css', array(), '1.0', 'all');
    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css',array('bootstrap','bootstrap-theme'),'all' );
    wp_enqueue_style( 'wpestate-child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );
    
   }    
    
endif;
add_action( 'wp_enqueue_scripts', 'wpestate_chld_thm_cfg_parent_css' );
load_child_theme_textdomain('wprentals', get_stylesheet_directory().'/languages');
// END ENQUEUE PARENT ACTION
// Code to rename properties to Boats

function change_property_label() {
    global $wp_post_types;
    $labels = &$wp_post_types['estate_property']->labels;
   
    $labels->name = 'Boats';
    $labels->singular_name = 'Boat';
    $labels->add_new = 'Add Boat';
    $labels->add_new_item = 'Add Boat';
    $labels->edit_item = 'Edit Boat';
    $labels->new_item = 'Boat';
    $labels->all_items = 'All Boats';
    $labels->view_item = 'View Boat';
    $labels->search_items = 'Search Boat';
    $labels->not_found = 'No Boat found';
    $labels->not_found_in_trash = 'No Boat found in Trash';    
}
add_action( 'init', 'change_property_label', 999 );

//code to Rename Owners to Marinas

function change_owner_label() {
    global $wp_post_types;
    $labels = &$wp_post_types['estate_agent']->labels;
    $labels->name = 'Marinas';
    $labels->singular_name = 'Marina';
    $labels->add_new = 'Add Marina';
    $labels->add_new_item = 'Add Marina';
    $labels->edit_item = 'Edit Marina';
    $labels->new_item = 'Marina';
    $labels->all_items = 'All Marina';
    $labels->view_item = 'View Marina';
    $labels->search_items = 'Search Marina';
    $labels->not_found = 'No Marina found';
    $labels->not_found_in_trash = 'No Marina found in Trash';    
}
add_action( 'init', 'change_owner_label', 999 );

// code to change label in menu 
function change_post_menu_label() {
    global $menu;
      
    //print_r($menu); //Print menus and find out the index of your custom post type menu from it.
      
    $menu[26][0] = 'Boats'; // Replace the 27 with your custom post type menu index from displayed above $menu array 
    $menu[27][0] = 'Marinas'; // Replace the 27 with your custom post type menu index from displayed above $menu array 
  }
  add_action( 'admin_menu', 'change_post_menu_label' );