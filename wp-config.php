<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'newdocklyne' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '}%#mmlNmf&9&I3wlQZTc1kHD@$$a@!Qqx8#{q>^BW[7w?7z^*pm}`t`IXM8$d!Ch' );
define( 'SECURE_AUTH_KEY',  'b~qWGW46aZ]u3pfj0PZbuFKQOMm@Y&hFWQ`>~,vvqpWv6I49U4JeL:`_|<8pCID7' );
define( 'LOGGED_IN_KEY',    'z!>I/R/ud(_+0d3fm;0]u(bQ}HcYe@*Y63Q>%+1?J6NmQ7x A;2K;hBl@{GXb|$,' );
define( 'NONCE_KEY',        'N*qN[azgj9``@u_z^!a 7d::P6-whX;D*9`C`K$2uV5K7?=7n/y7LGa&zV`/(lc7' );
define( 'AUTH_SALT',        'xQNb@N<>F1[Cz(PHA#|06%Hd28Craw1Vt]f8/k]H+QZtx{.K9vqL$!wgY8O3mO3g' );
define( 'SECURE_AUTH_SALT', '>A-/ l>mC`PFQgs]MCU<60CN]3I-I}oMAr3YR4XLZr)5fgnc7/ RnI>`8dfWBw8S' );
define( 'LOGGED_IN_SALT',   'Q,ot/iQhttdG5D)_Qee^`GXMb!Q385R%*-s%?@-9r%g-z^vV>A!O=p33evFj5BJA' );
define( 'NONCE_SALT',       'ray$7Xi>43whd#ok#W$4Q+7ABio*nAQA^XN%$#~KxEkh)-!@Pd-!u:Jo>qUeZ<mH' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wpdock_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );

//increase memory limit as per theme recommendation 
define( 'WP_MEMORY_LIMIT', '96M' );

